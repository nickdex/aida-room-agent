#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#include <PubSubClient.h>
#include <ArduinoJson.h>

#define OUT_TOPIC "arduino"
#define IN_TOPIC "server"
#define PRESENCE_TOPIC "presence"

#define FIRMWARE_SERVER_URL "192.168.0.100"
#define FIRMWARE_SERVER_PORT 7000
#define FIRMWARE_SERVER_PATH "/update"

#define MQTT_URL "192.168.0.100"
#define MQTT_PORT 1883
#define MQTT_USER ""
#define MQTT_PASS ""

#define AGENT_ID "nikhil-home-nikhilroom-test"

#define PIN5 5
#define PIN4 4
#define PIN2 2
#define PIN0 0

WiFiClient espClient;
PubSubClient client(espClient);

bool isUpdating = false;

void updateFirmware()
{
    isUpdating = true;
}

void setupWifi()
{

    WiFiManager wifiManager;
    //reset saved settings
    // wifiManager.resetSettings();

    wifiManager.autoConnect("AutoConnectAP");

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

void handleMessage(char *topic, byte *payload, unsigned int length)
{
    char json[length];
    DynamicJsonDocument doc(length);

    Serial.print("Message from topic: ");
    Serial.println(topic);

    // Convert to char
    for (u16_t i = 0; i < length; i++)
    {
        json[i] = (char)payload[i];
    }

    // Deserialize the JSON document
    DeserializationError error = deserializeJson(doc, json);

    // Test if parsing succeeds.
    if (error)
    {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
    }

    // Parse JSON
    serializeJsonPretty(doc, Serial);
    Serial.println();

    // Execute Action
    bool resError = false;

    const char *agentId = doc["agentId"];
    if (strcmp(AGENT_ID, agentId) != 0)
    {
        return;
    }

    const size_t capacity = JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + 90;
    DynamicJsonDocument responseDoc(capacity);

    // Message handling
    bool isUpdate = doc["isUpdate"];
    if (isUpdate)
    {
        updateFirmware();
        return;
    }

    int devicePin = doc["device"];
    const char *action = doc["action"];

    if (strcmp(action, "on") == 0)
    {
        digitalWrite(devicePin, HIGH);
    }
    else if (strcmp(action, "off") == 0)
    {
        digitalWrite(devicePin, LOW);
    }
    else
    {
        resError = true;
    }
    // Prepare response body

    JsonObject response = responseDoc.createNestedObject("response");
    response["isError"] = resError;
    responseDoc["request"] = doc.as<JsonObject>();

    // Publish Response
    char serializedResponse[300];
    serializeJson(responseDoc, serializedResponse);

    client.publish(OUT_TOPIC, serializedResponse);
}

void setup()
{
    pinMode(PIN0, OUTPUT);
    pinMode(PIN2, OUTPUT);
    pinMode(PIN4, OUTPUT);
    pinMode(PIN5, OUTPUT);

    Serial.begin(115200);
    setupWifi();
    client.setServer(MQTT_URL, MQTT_PORT);
    client.setCallback(handleMessage);
}

void reconnect()
{
    // Loop until we're reconnected
    while (!client.connected())
    {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect(AGENT_ID))
        {
            Serial.println("connected");
            // Once connected, publish an announcement...
            client.publish(PRESENCE_TOPIC, AGENT_ID);
            // ... and resubscribe
            client.subscribe(IN_TOPIC);
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void loop()
{
    if (isUpdating)
    {
        // Http update code needs to be in loop function otherwise it doesn't work
        Serial.println("Going to update firmware...");
        if ((WiFi.status() == WL_CONNECTED))
        {

            Serial.println("Checking for Update.");
            auto ret = ESPhttpUpdate.update(FIRMWARE_SERVER_URL, FIRMWARE_SERVER_PORT, FIRMWARE_SERVER_PATH);

            switch (ret)
            {
            case HTTP_UPDATE_FAILED:
                Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
                break;

            case HTTP_UPDATE_NO_UPDATES:
                Serial.println("HTTP_UPDATE_NO_UPDATES");
                break;

            case HTTP_UPDATE_OK:
                Serial.println("HTTP_UPDATE_OK");
                break;
            }
            isUpdating = false;
        }
    }
    else
    {
        if (!client.connected())
        {
            reconnect();
        }
        client.loop();
    }
}
